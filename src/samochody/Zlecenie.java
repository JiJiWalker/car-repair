package samochody;

import java.math.BigDecimal;

/**
 * Created by Jakub on 07.06.2017.
 */
public class Zlecenie<T extends Samochód> {
    private int nrZlecenia;
    private String imię;
    private String nazwisko;
    private String nrTelefonu;
    private String nrDowoduRejestracyjnego;
    private T samochód; // chodzi o atrybut samochód z samochodowymi konstrutkorami
    // typ enum, ale on w konstruktorze ma być niepotrzebny, ponieważ dopiero przy zmianie są opcje konkretne. Standardowo jest nowy
    private String nazwiskoMechanika;
    private BigDecimal cenaZaUsługę;
    private String opisNaprawy;

    StatusNaprawy status = StatusNaprawy.NOWE_ZLECENIE;

    public int getNrZlecenia() {
        return nrZlecenia;
    }

    public void setNrZlecenia(int nrZlecenia) {
        this.nrZlecenia = nrZlecenia;
    }

    public String getImię() {
        return imię;
    }

    public void setImię(String imię) {
        this.imię = imię;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getNrTelefonu() {
        return nrTelefonu;
    }

    public void setNrTelefonu(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }

    public String getNrDowoduRejestracyjnego() {
        return nrDowoduRejestracyjnego;
    }

    public void setNrDowoduRejestracyjnego(String nrDowoduRejestracyjnego) {
        this.nrDowoduRejestracyjnego = nrDowoduRejestracyjnego;
    }

    public T getSamochód() {
        return samochód;
    }

    public void setSamochód(T samochód) {
        this.samochód = samochód;
    }

    public String getNazwiskoMechanika() {
        return nazwiskoMechanika;
    }

    public void setNazwiskoMechanika(String nazwiskoMechanika) {
        this.nazwiskoMechanika = nazwiskoMechanika;
    }

    public BigDecimal getCenaZaUsługę() {
        return cenaZaUsługę;
    }

    public void setCenaZaUsługę(BigDecimal cenaZaUsługę) {
        this.cenaZaUsługę = cenaZaUsługę;
    }

    public String getOpisNaprawy() {
        return opisNaprawy;
    }

    public void setOpisNaprawy(String opisNaprawy) {
        this.opisNaprawy = opisNaprawy;
    }

    public StatusNaprawy getStatus() {
        return status;
    }

    public void setStatus(StatusNaprawy status) {
        this.status = status;
    }

    //this.cena = new BigDecimal(123);
    //cena.add(new BigDecimal(23));

    public Zlecenie(int nrZlecenia, String imię, String nazwisko) {
        this.nrZlecenia = nrZlecenia;
        this.imię = imię;
        this.nazwisko = nazwisko;
    }

    public Zlecenie() {
    }

//    public Zlecenie(T samochod) {
//        this.samochod = samochod;
//    }

    public Zlecenie(StatusNaprawy status) {
        this.status = status;
    }

    public Zlecenie(int nrZlecenia, String imię, String nazwisko, String nrTelefonu,
                    String nrDowoduRejestracyjnego, T samochód, String nazwiskoMechanika) {
        this.nrZlecenia = nrZlecenia;
        this.imię = imię;
        this.nazwisko = nazwisko;
        this.nrTelefonu = nrTelefonu;
        this.nrDowoduRejestracyjnego = nrDowoduRejestracyjnego;
        this.samochód = samochód;
        this.nazwiskoMechanika = nazwiskoMechanika;
    }

    public Zlecenie(int nrZlecenia, String imię, String nazwisko, String nrTelefonu, String nrDowoduRejestracyjnego,
                    T samochód, String nazwiskoMechanika, BigDecimal cenaZaUsługę, String opisNaprawy, StatusNaprawy status) {
        this.nrZlecenia = nrZlecenia;
        this.imię = imię;
        this.nazwisko = nazwisko;
        this.nrTelefonu = nrTelefonu;
        this.nrDowoduRejestracyjnego = nrDowoduRejestracyjnego;
        this.samochód = samochód;
        this.nazwiskoMechanika = nazwiskoMechanika;
        this.cenaZaUsługę = cenaZaUsługę;
        this.opisNaprawy = opisNaprawy;
        this.status = status;
    }

    public void wyswietl() {
        System.out.println("Nr zlecenia: " + this.getNrZlecenia());
    }
    public void zmienStatus(int i) {
        this.status = StatusNaprawy.fromInt(i);
    }

    @Override
    public String toString() {
        return "\n"+"Zlecenie nr. "+nrZlecenia+"\n" +
                "Imię: " + imię +"\n" +
                "Nazwisko: " + nazwisko + "\n" +
                "Nr. Tel.: " + nrTelefonu + "\n" +
                "Nr. Dowodu Rejestracynego: " + nrDowoduRejestracyjnego + "\n" +
                "" + samochód +
                //", samochod=" + samochod +
                "Nazwisko mechanika: " + nazwiskoMechanika + "\n" +
                "Cena za usługę: " + cenaZaUsługę + "\n" +
                "Opis naprawy: " + opisNaprawy + "\n" +
                "Status naprawy: " + status + "\n";
    }
}
