package samochody;

/**
 * Created by Jakub on 07.06.2017.
 */
public class SamochódCiężarowy extends Samochód {

    private String carMileage;

    public String getCarMileage() {
        return carMileage;
    }

    public void setCarMileage(String carMileage) {
        this.carMileage = carMileage;
    }

    public SamochódCiężarowy() {
    }

    public SamochódCiężarowy(String mark, String model, String colour, int productionYear, String carMileage) {
        super(mark, model, colour, productionYear);
        this.carMileage = carMileage;
    }

    @Override
    public String toString() {
        return "Samochód ciężarowy" + "\n" +
                "Marka: "+ mark + "\n" +
                "Model: "+ model + "\n" +
                "Kolor: "+ colour + "\n" +
                "Rok produkcji: "+ productionYear + "\n" +
                "Przebieg: " + carMileage + "\n";
    }
}
