package samochody;

/**
 * Created by Jakub on 07.06.2017.
 */
public class SamochódOsobowy extends Samochód {

    private double engineCapacity;

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public SamochódOsobowy() {
    }

    public SamochódOsobowy(String mark, String model, String colour, int productionYear, double engineCapacity) {
        super(mark, model, colour, productionYear);
        this.engineCapacity = engineCapacity;
    }

    @Override
    public String toString() {
        return "Samochód osobowy" + "\n" +
                "Marka: "+ mark + "\n" +
                "Model: "+ model + "\n" +
                "Kolor: "+ colour + "\n" +
                "Rok produkcji: "+ productionYear + "\n" +
                "Pojemność silnika: " + engineCapacity + "\n";
    }
}
