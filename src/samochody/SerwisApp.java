package samochody;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Jakub on 07.06.2017.
 */
public class SerwisApp {
    private static final int WYJDŹ = 0;

    private Serwis serwis;

    private SerwisApp() {
        serwis = new Serwis();
    }


    public static void main(String[] args) {

        SerwisApp serwisApp = new SerwisApp();

        serwisApp.serwis.zlecenieMap.put(10, new Zlecenie(10, "Ktoś", "Coś", "123", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 1912, "13KM"), "Alan"));
        serwisApp.serwis.zlecenieMap.put(12, new Zlecenie(12, "Ktoś1", "Coś2", "1233", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 19121, "12KM"), "Balan"));
        serwisApp.serwis.zlecenieMap.put(13, new Zlecenie(13, "Ktoś", "Coś", "123", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 19122, "13KM"), "Alan"));
        serwisApp.serwis.zlecenieMap.put(14, new Zlecenie(14, "Ktoś", "Coś", "123", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 191322, "12KM"), "Calan"));
        serwisApp.serwis.zlecenieMap.put(15, new Zlecenie(15, "Ktoś", "Coś", "123", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 19112, "13KM"), "Boicz"));
        serwisApp.serwis.zlecenieMap.put(16, new Zlecenie(16, "Ktoś", "Coś", "123", "AB1",
                new SamochódCiężarowy("Marka", "Model", "Kolor", 1912, "12KM"), "Calan"));
        serwisApp.serwis.zlecenieMap.put(20, new Zlecenie(16, "Ktoś", "Coś", "123", "AB1",
                new SamochódOsobowy("Marka", "Model", "Kolor", 2000, 1.1), "Zalan"));
        serwisApp.serwis.zlecenieMap.put(21, new Zlecenie(16, "Ktoś", "Coś", "123", "AB1",
                new SamochódOsobowy("Marka", "Model", "Kolor", 2000, 2.2), "Zalan"));
        System.out.println(serwisApp.serwis.zlecenieMap.keySet());

        try (Scanner sc = new Scanner(System.in)) {
            int userOption;
            do {
                serwisApp.opcje();
                userOption = sc.nextInt();
                switch (userOption) {
                    case 1:
                        serwisApp.serwis.wyświetlZlecenia(serwisApp.serwis.zlecenieMap);
                        break;
                    case 2:
                        serwisApp.serwis.argumentyZlecenia(sc);
                        break;
                    case 3:
                        serwisApp.zmianaStatusuNaprawy(sc);
                        break;
                    case 4:
                        serwisApp.wyświetlZleceniaZakończone();
                        break;
                    case 5:
                        serwisApp.wyświetlZleceniaWTrakcie();
                        break;
                    case 6:
                        serwisApp.wyświetlZleceniaPrzyjęte();
                        break;
                    case 7:
                        serwisApp.wyświetlZleceniaOdrzucone();
                        break;
                    case 8:
                        serwisApp.serwis.posortujZleceniaWgSilnika();
                        break;
                    case 9:
                        serwisApp.serwis.posortujZleceniaPrzebiegu();
                    case 10:
                        serwisApp.wyświetlUsuńZlecenie(sc);
                        break;
                    case 11:
                        serwisApp.wyświetlZleceniaMechanika(sc);
                        break;
//                    case 12:
//                        serwis.gotowiecTrzech(sc);
//                        break;
                }
            } while (userOption != SerwisApp.WYJDŹ);
        } catch (NullPointerException s) {
            s.printStackTrace();
        } catch (InputMismatchException s) {
            System.err.println("Niepoprawny format");
        }
    }

    public void opcje() {
        System.out.println("\t"+"***** Car Repair System v1.0 *****"+"\n");
        System.out.println("1. Pokaż wszystkie zlecenia");
        System.out.println("2. Dodaj zlecenie.");
        System.out.println("3. Zmien status zlecenia");
        System.out.println("4. Wyświetl zlecenia zakończone");
        System.out.println("5. Wyświetl zlecenia w trakcie");
        System.out.println("6. Wyświetl zlecenia przyjęte do realizacji");
        System.out.println("7. Wyświetl zlecenia odrzucone");
        System.out.println("8. [in progrss] Wyświetl zlecenia posortowane wg pojemności silnika");
        System.out.println("9. [in progress] Wyświetl zlecenia posortowane wg przebiegu");
        System.out.println("10. Usuń zlecenie o danym numerze");
        System.out.println("11. Wyświetl zlecenia dla danego mechanika");
    }

    public void zmianaStatusuNaprawy(Scanner sc) {

            int n, m;
            System.out.println("Wybierz zlecenie do zmiany: "+"\n"+serwis.zlecenieMap.keySet());
            n = sc.nextInt();
            sc.nextLine();
            Zlecenie z = serwis.zlecenieMap.get(n);
            System.out.println("Aktualny status naprawy: " +serwis.zlecenieMap.get(n).getStatus()+"\n");
            System.out.println("Ustaw status naprawy na:");
            System.out.println("1. Zakończona");
            System.out.println("2. W trakcie");
            System.out.println("3. Przyjęta do realizacji");
            System.out.println("4. Odrzucona");
            m = sc.nextInt();
            sc.nextLine();
            z.setStatus(StatusNaprawy.fromInt(m));
            if (StatusNaprawy.fromInt(m) == StatusNaprawy.ZAKOŃCZONA) {
                BigDecimal cena;
                String opis;
                System.out.println("Opisz zmiany bo naprawie");
                opis = sc.nextLine();
                z.setOpisNaprawy(opis);
                System.out.println("Podaj cenę naprawy");
                cena = sc.nextBigDecimal();
                z.setCenaZaUsługę(cena);
                z.setStatus(StatusNaprawy.fromInt(m));
             }
        System.out.println("Nowy status zlecenia: "+serwis.zlecenieMap.get(n).getStatus()+"\n");
    }

    public void wyświetlZleceniaZakończone() {
        System.out.println("Zlecenia zakończone");
        serwis.wyswietlZlecenia(StatusNaprawy.ZAKOŃCZONA);
    }

    public void wyświetlZleceniaWTrakcie() {
        System.out.println("Zlecenia w trakcie");
        serwis.wyswietlZlecenia(StatusNaprawy.W_TRAKCIE);
    }

    public void wyświetlZleceniaPrzyjęte() {
        System.out.println("Zlecenia przyjęte");
        serwis.wyswietlZlecenia(StatusNaprawy.PRZYJĘTA_DO_REALIZACJI);
    }

    public void wyświetlZleceniaOdrzucone() {
        System.out.println("Zlecenia odrzucone");
        serwis.wyswietlZlecenia(StatusNaprawy.ODRZUCONA);
    }

    public void wyświetlZleceniaMechanika(Scanner sc) {
        System.out.println("Zlecenia mechanika: ");
        String mech;
        mech = sc.nextLine();
        serwis.zleceniaMechanika(mech);
    }

    public void wyświetlUsuńZlecenie(Scanner sc) {
        int n;
        System.out.println("Jakie zlecenie chcesz usunąć?");
        System.out.println(serwis.zlecenieMap.keySet());
        n = sc.nextInt();
        serwis.usuńZlecenie(n);

    }



}
