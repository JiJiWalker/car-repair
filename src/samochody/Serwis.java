package samochody;

import java.util.*;


/**
 * Created by Jakub on 07.06.2017.
 */
public class Serwis {
    private static final int OSOBOWY = 1;
    private static final int CIĘŻAROWY = 2;
    private Scanner sc = new Scanner(System.in);

    public Map<Integer, Zlecenie> zlecenieMap = new HashMap<>();

     public Serwis() {
         new HashMap<>();
    }

    private Serwis serwis;

    public void wyświetlZlecenia(Map<Integer, Zlecenie> zlecenieMap) {
        if (zlecenieMap.isEmpty()) {
            System.out.println("Brak zleceń");
        } else {
            System.out.println(zlecenieMap.values());
        }
    }

// Do zmiejszenia linijek w metodzie zmianastatusunaprawy
//    public Zlecenie zmianaStatusuNaprawy(int n) {
//        Zlecenie<Samochód> s = new Zlecenie<>();
//        System.out.println("Zmień status zlecenia");
//        System.out.println("Ustaw status na:");
//        System.out.println("1. Zakończona");
//        System.out.println("2. W trakcie");
//        System.out.println("3. Przyjęta do realizacji");
//        System.out.println("4. Odrzucona");
//
//
//        return new Zlecenie(s.getStatus());
////        System.out.println("1. Zakończona");
////        System.out.println("2. W trakcie");
////        System.out.println("3. Przyjęta do realizacji");
////        System.out.println("4. Odrzucona");
//    }

    public void wybierzSamochód() {
        System.out.println("1. Samochód osobowy?");
        System.out.println("2. Samochód ciężarowy?");
        int n = sc.nextInt();

        SamochódOsobowy samOs = new SamochódOsobowy();
        SamochódCiężarowy samCi = new SamochódCiężarowy();

        /*if (n == Serwis.OSOBOWY) {
            serwis.argumentySamochoduOsobowego();
        } else if (n == Serwis.CIĘŻAROWY) {
            serwis.argumentySamochoduCiężarowego();
        }*/
    }

    public Zlecenie argumentyZlecenia(Scanner sc) {
        System.out.println("Nr. zlecenia");
        int nrZlecenia = sc.nextInt();
        sc.nextLine();
        System.out.println("Imię");
        String imię = sc.nextLine();
        System.out.println("Nazwisko");
        String nazwisko = sc.nextLine();
        System.out.println("Telefon");
        String telefon = sc.nextLine();
        System.out.println("Nr. dowodu rejestracyjnego");
        String nrDowRej = sc.nextLine();
        System.out.println("1. Samochód osobowy.");
        System.out.println("2. Samochód ciężarowy.");
        int n = sc.nextInt();
        sc.nextLine();
        Samochód s = null;

        if (n == Serwis.OSOBOWY) {
            System.out.println("Marka");
            String marka = sc.nextLine();
            System.out.println("Model");
            String model = sc.nextLine();
            System.out.println("Kolor");
            String kolor = sc.nextLine();
            System.out.println("Rok produkcji");
            int rokProd = sc.nextInt();
            sc.nextLine();
            System.out.println("Pojemność silnika");
            double pojemność = sc.nextDouble();
            sc.nextLine();
            s = new SamochódOsobowy(marka, model, kolor, rokProd, pojemność);
        } else if (n == Serwis.CIĘŻAROWY) {
            System.out.println("Marka");
            String marka = sc.nextLine();
            System.out.println("Model");
            String model = sc.nextLine();
            System.out.println("Kolor");
            String kolor = sc.nextLine();
            System.out.println("Rok produkcji");
            int rokProd = sc.nextInt();
            sc.nextLine();
            System.out.println("Przebieg");
            String przebieg = sc.nextLine();
            s = new SamochódCiężarowy(marka, model, kolor, rokProd, przebieg);
        } else {
            System.out.println("Naprawda bez samochodu. Zły If.");
        }

        System.out.println("Nazwisko mechanika");
        String nazMechanika = sc.nextLine();
        System.out.println("\n"+"Dodano zlecenie");

        return zlecenieMap.put(nrZlecenia, new Zlecenie<>(nrZlecenia, imię, nazwisko, telefon, nrDowRej, s, nazMechanika)) ;
    }

    // ta metoda oraz poniższa wyrzuca mi nullpointerexception
//    public SamochódOsobowy argumentySamochoduOsobowego() throws NullPointerException{
//        System.out.println("Marka");
//        String marka = sc.nextLine();
//        System.out.println("Model");
//        String model = sc.nextLine();
//        System.out.println("Kolor");
//        String kolor = sc.nextLine();
//        System.out.println("Rok produkcji");
//        int rokProd = sc.nextInt();
//        sc.nextLine();
//        System.out.println("Pojemność silnika");
//        double pojemność = sc.nextDouble();
//
//        System.out.println("Dodano samochód osobowy");
//
//        return new SamochódOsobowy(marka, model, kolor, rokProd, pojemność);
//    }
//
//    public SamochódCiężarowy argumentySamochoduCiężarowego(Scanner sc) throws NullPointerException{
//        System.out.println("Marka");
//        String marka = sc.nextLine();
//        System.out.println("Model");
//        String model = sc.nextLine();
//        System.out.println("Kolor");
//        String kolor = sc.nextLine();
//        System.out.println("Rok produkcji");
//        int rokProd = sc.nextInt();
//        sc.nextLine();
//        System.out.println("Przebieg");
//        String przebieg = sc.nextLine();
//
//        System.out.println("Dodano samochód ciężarowy");
//
//        return new SamochódCiężarowy(marka, model, kolor, rokProd, przebieg);
//    }

    /*public void dodajZlecenie(int key, Map<Integer, Zlecenie> zlecenieMap) {
         zlecenieMap.put(key, serwis.argumentyZlecenia());
    }*/


//        zlecenieMap.put(1, new samochody.Zlecenie(1, "Jan", "Kowalski", "123-123-123",
//                "ADA22", new samochody.SamochódOsobowy("Audi", "TT", "Biały", 2010, 1.6),
//                "Woźniak" ));

//    public samochody.Serwis() {
//        zlecenieMap.put(1, new samochody.Zlecenie(1, "Jan", "Kowalski", "123-123-123",
//                "ADA22", new samochody.SamochódOsobowy("Audi", "TT", "Biały", 2010, 1.1),
//                "Woźniak" ));
//        zlecenieMap.put(2, new samochody.Zlecenie(samochody.StatusNaprawy.NOWE_ZLECENIE));
//
//    }

//    public void dodajZlecenie() {
//        boolean czyOsobowy = false;
//        samochody.Zlecenie z;
//        int numerZlecenia = 1;
//
//        if (czyOsobowy) {
//            z = new samochody.Zlecenie<>(new samochody.SamochódOsobowy());
//        } else {
//            z = new samochody.Zlecenie<>(new samochody.SamochódCiężarowy());
//        }
//        z.getSamochod();
//
//
//        samochody.Zlecenie<samochody.SamochódOsobowy> zz = new samochody.Zlecenie<>(new samochody.SamochódOsobowy());
//        samochody.Zlecenie<samochody.SamochódCiężarowy> zc = new samochody.Zlecenie<>(new samochody.SamochódCiężarowy());
//        zc.setSamochod(new samochody.SamochódCiężarowy());
//
//
//        /*wyswietlZlecenia(samochody.StatusNaprawy.NOWE_ZLECENIE);
//        this.zlecenieMap.put(1,z);*/
//    }

    public void posortujZleceniaWgSilnika() {
        List<samochody.Zlecenie<samochody.SamochódOsobowy>> zleceniaSamochodowOsobowych = new ArrayList<>();

        this.zlecenieMap.values().forEach(zlecenie -> {
            if (zlecenie.getSamochód().getClass().getName().equals(SamochódOsobowy.class.getName()))
                zleceniaSamochodowOsobowych.add(zlecenie);
        });

        zleceniaSamochodowOsobowych.sort(Comparator.comparingDouble(left -> left.getSamochód().getEngineCapacity()));
        zleceniaSamochodowOsobowych.forEach(Zlecenie::wyswietl);
    }

    public void posortujZleceniaPrzebiegu() {
        List<samochody.Zlecenie<samochody.SamochódCiężarowy>> zleceniaSamochodowCiezarowy = new ArrayList<>();

        this.zlecenieMap.values().forEach(zlecenie -> {
            try {
                if (zlecenie.getSamochód().getClass().isInstance(SamochódCiężarowy.class))
                    zleceniaSamochodowCiezarowy.add(zlecenie);
            } catch(NullPointerException ne) {
                System.out.println("Zlecenie "+zlecenie.getNrZlecenia()+" nie ma przypisanego samochodu");
            }
        });

        zleceniaSamochodowCiezarowy.sort(Comparator.comparing(left -> left.getSamochód().getCarMileage()));
    }

    public void wyświetl() {
        System.out.println(zlecenieMap.values());
    }

    void wyswietlZlecenia(samochody.StatusNaprawy statusNaprawy) {
        this.zlecenieMap.values().forEach(zlecenie -> {
            if(zlecenie.status.equals(statusNaprawy)) {
                System.out.println(zlecenieMap.get(zlecenie.getNrZlecenia()));
            }
        });
   }

   void zleceniaMechanika(String g) {
       Scanner sc = new Scanner(System.in);
       g = sc.nextLine();
       String finalG = g;
       this.zlecenieMap.values().forEach(zlecenie ->  {
           if (zlecenie.getNazwiskoMechanika().equals(finalG)) {
                zlecenie.wyswietl();
            }
        });
    }

   public void usuńZlecenie(int n) {

        if  (zlecenieMap.get(n) == null) {
            System.out.println("Nie ma takiego zlecenia");
        } else {
            this.zlecenieMap.remove(n);
            System.out.println("Zlecenie usunięto.");
        }

   }
//


    }