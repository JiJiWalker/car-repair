package samochody;

/**
 * Created by Jakub on 07.06.2017.
 */
public abstract class Samochód {

    protected String mark;
    protected String model;
    protected String colour;
    protected int productionYear;

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(int productionYear) {
        this.productionYear = productionYear;
    }

    public Samochód() {
    }

    public Samochód(String mark, String model, String colour, int productionYear) {
        this.mark = mark;
        this.model = model;
        this.colour = colour;
        this.productionYear = productionYear;
    }
}
