package samochody;

/**
 * Created by Jakub on 07.06.2017.
 */
public enum StatusNaprawy {
    NOWE_ZLECENIE("Nowe zlecenie."), ZAKOŃCZONA("Zakończona."), W_TRAKCIE("W trakcie."), PRZYJĘTA_DO_REALIZACJI("Przyjęta do realizacji"),
    ODRZUCONA("Odrzucona");

    String status;

    StatusNaprawy(String status) {
        this.status = status;
    }

    public static StatusNaprawy fromInt(int n) {
        switch (n) {
            case 1:
                return ZAKOŃCZONA;
            case 2:
                return W_TRAKCIE;
            case 3:
                return PRZYJĘTA_DO_REALIZACJI;
            case 4:
                return ODRZUCONA;
            default:
                return NOWE_ZLECENIE;
        }
    }

    @Override
    public String toString() {
        return status;
    }
}
